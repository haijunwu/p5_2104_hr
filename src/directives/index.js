// 处理图片加载失败的自定义指令
export const imageerror = {
  // 钩子函数 当绑定的元素插入父节点的时候触发
  inserted(dom, options) {
    // 当dom节点也就是图片 加载失败的时候触发
    dom.onerror = function() {
      dom.src = options.value
    }
  }
}
// input 聚焦的指令
export const inputfocus = {
  // 钩子函数 当绑定的元素插入父节点的时候触发
  inserted(dom, options) {
    // 当dom节点也就是图片 加载失败的时候触发
    dom.onerror = function() {
      dom.src = options.value
    }
  }
}

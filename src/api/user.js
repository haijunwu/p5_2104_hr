import request from '@/utils/request'

// 封装的登录请求接口
export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}
// 获取用户信息的接口
export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

// 获取员工详情信息接口
export function getUserDetailById(id){
  return request({
    url:`/sys/user/${id}`,
    method:'get'
  })
}

export function getInfo(token) {}

export function logout() {}

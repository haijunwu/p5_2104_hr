import { getToken, setToken, removeToken } from '@/utils/auth'
// 在src/api/user的里的login方法导入到这里使用
import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { resetRouter } from '@/router'
const state = {
  token: getToken(),
  userInfo: {}
}

const mutations = {
  setToken(state, token) {
    state.token = token // 修改state的token
    setToken(token) // 放到缓存里
  },
  removeToken(state) {
    state.token = null // 释放vuex state里的 token
    removeToken() // 清空缓存的token
  },
  setUserInfo(state, userInfo) {
    state.userInfo = userInfo // 第一种写法
    // state.userInfo = { ... userInfo }
  },
  removeUserInfo() {
    state.userInfo = {}
  }
}

const actions = {
  async login(context, data) {
    // 在action里调用 登录接口 data 是传参
    const result = await login(data)
    console.log('result:', result) // result 就是token
    // 触发mutatinos里的 setToken的方法
    context.commit('setToken', result)
  },
  // aciton 在这里去写调用获取用户信息的接口
  async getUserInfo(context) {
    // 请求用户信息接口
    const result = await getUserInfo()
    const baseInfo = await getUserDetailById(result.userId)
    // console.log('getUserInfo result:', result)
    // console.log('baseInfo:', baseInfo)
    // 将两个接口的数据 合并成一个对象里
    const baseResult = { ...result, ...baseInfo }
    // 提交给mutation去修改数据
    context.commit('setUserInfo', baseResult)
    return baseResult
  },
  // 退出登录的action
  logout(context) {
    // 清除token
    context.commit('removeToken')
    // 清除用户信息
    context.commit('removeUserInfo')
    resetRouter()
    // 还有一步  vuex中的数据是不是还在
    // 要清空permission模块下的state数据
    // vuex中 user子模块  permission子模块
    // 子模块调用子模块的action  默认情况下 子模块的context是子模块的
    // 父模块 调用 子模块的action
    context.commit('permission/setRoutes', [], { root: true })
    // 子模块调用子模块的action 可以 将 commit的第三个参数 设置成  { root: true } 就表示当前的context不是子模块了 而是父模块
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

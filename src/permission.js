import router from '@/router' // 引入路由实例
// vue中引用 页面的进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import store from '@/store'

const whiteList = ['/login', '/404']
// 路由的前置守卫
router.beforeEach(async function(to, from, next) {
  NProgress.start() // 开启进度条
  if (store.getters.token) {
    if (to.path === '/login') {
      next('/') // 已经登陆了 免登录 跳到主页
    } else {
      if (!store.state.user.userInfo.userId) {
        const { roles } = await store.dispatch('user/getUserInfo')

        // 筛选用户的可用路由
        // actions中函数 默认是Promise对象 调用这个对象 想要获取返回的值话 必须 加 await或者是then
        // actions是做异步操作的
        const routes = await store.dispatch(
          'permission/filterRoutes',
          roles.menus
        )
        // routes就是筛选得到的动态路由
        // 动态路由 添加到 路由表中 默认的路由表 只有静态路由 没有动态路由
        // addRoutes  必须 用 next(地址) 不能用next()
        router.addRoutes([
          ...routes,
          { path: '*', redirect: '/404', hidden: true }
        ]) // 添加动态路由到路由表  铺路
        // 添加完动态路由之后
        next(to.path) // 相当于跳到对应的地址  相当于多做一次跳转 为什么要多做一次跳转
        // 进门了，但是进门之后我要去的地方的路还没有铺好，直接走，掉坑里，多做一次跳转，再从门外往里进一次，跳转之前 把路铺好，再次进来的时候，路就铺好了
      }
      next() // 直接放行
    }
  } else {
    // 是否在白名单里面
    if (whiteList.indexOf(to.path) > -1) {
      next()
    } else {
      next('/login') // 没有登录，也不在白名单里，就去登录
    }
  }

  NProgress.done() // 手动关闭进度条
})

//  后置导航守卫
router.afterEach(function() {
  NProgress.done()
})
